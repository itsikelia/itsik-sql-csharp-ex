﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
//using Microsoft.Office.Interop.Excel;

namespace CSharpAndSQLEx_Itsik
{
    
    public partial class Form1 : Form
    {
        public static DbServiceSQL dbService = new DbServiceSQL();
        List<Part> parts=new List<Part>();
        List<Machine> machines = new List<Machine>();
        List<WorkOrder> WorkOrders = new List<WorkOrder>();
        public Form1()
        {
            InitializeComponent();
            //   foreach(DataRow data in dbService.executeSelectQueryNoParam($@"SELECT * FROM PartItsik").AsEnumerable().ToList())
            //     parts.Add(new Part((int)data.ItemArray[3], (int)data.ItemArray[4],(DateTime) data.ItemArray[2], (int)data.ItemArray[0],(string)data.ItemArray[1]));
            //   foreach (DataRow data in dbService.executeSelectQueryNoParam($@"SELECT * FROM MachinesItsik").AsEnumerable().ToList())
            //       machines.Add(new Machine((int)data.ItemArray[2], (int)data.ItemArray[3], (string)data.ItemArray[0],(DateTime)data.ItemArray[1]));

            updateListOfMachine();
           
            


        }

        private void partButton_Click(object sender, EventArgs e)
        {

            
            int x;
            if (!int.TryParse(userCodeTB.Text, out x) || !int.TryParse(langCodeTB.Text, out x))
            {
                MessageBox.Show("!קוד משתמש וקוד שפה צריכים להיות מספרים בלבד");
                return;
            }



            if (checkIfCataologNumExist(int.Parse(catalogTB.Text)))
            {
                MessageBox.Show("מס' קטלוגי זה כבר נמצא במערכת");
                return;
            }



            //(catalog,desc,date,usercode,langcode)
            string query = $@"INSERT INTO PartItsik VALUES(?,?,?,?,?)";
            SqlParameter catalog = new SqlParameter("ca", int.Parse(catalogTB.Text));
            SqlParameter desc = new SqlParameter("de", partDescTB.Text);
            SqlParameter date = new SqlParameter("da", DateTime.Now.ToString("yyyy-MM-dd"));
            SqlParameter uc = new SqlParameter("uc", int.Parse(userCodeTB.Text));
            SqlParameter lc = new SqlParameter("lc", int.Parse(langCodeTB.Text));
            dbService.ExecuteQuery(query, CommandType.Text, catalog, desc,date, uc, lc);
            MessageBox.Show("פריט נוסף בהצלחה");
            partDescTB.Text = "";
            catalogTB.Text = "";

            
        }

        private void machineButton_Click(object sender, EventArgs e)
        {
            int x;
            if (!int.TryParse(userCodeTB.Text, out x) || !int.TryParse(langCodeTB.Text, out x))
            {
                MessageBox.Show("קוד משתמש וקוד שפה צריכים להיות מספרים בלבד!!");
                return;
            }
            if (checkIfMachineExist(machineNameTB.Text))
            {
                MessageBox.Show("מכונה זו כבר קיימת במערכת");
                return;
            }



            //TEST
            //(name,date,usercode,langcode)
            string query = $@"INSERT INTO MachineItsik VALUES(?,?,?,?)";
            SqlParameter machineName = new SqlParameter("ca", machineNameTB.Text);
            SqlParameter date = new SqlParameter("da", DateTime.Now.ToString("yyyy-MM-dd"));
            SqlParameter uc = new SqlParameter("uc", int.Parse(userCodeTB.Text));
            SqlParameter lc = new SqlParameter("lc", int.Parse(langCodeTB.Text));
            dbService.ExecuteQuery(query, CommandType.Text, machineName, date, uc, lc);
            MessageBox.Show("מכונה נוספה בהצלחה");
            machineNameTB.Text = "";
            updateListOfMachine();
        }

        private void workOrderButton_Click(object sender, EventArgs e)
        {
            if (!checkIfCataologNumExist(int.Parse(catalogOrderTB.Text)))
            {
                MessageBox.Show("מס' קטלוגי לא חוקי");
                return;
            }
           
            
            int x ;
            if(!int.TryParse(userCodeTB.Text,out x) || !int.TryParse(langCodeTB.Text, out x))
            {
                MessageBox.Show("קוד משתמש וקוד שפה צריכים להיות מספרים בלבד");
                return;
            }
            if (!int.TryParse(orderNumTB.Text, out x))
            {
                MessageBox.Show("מס הזמנה מורכב ממספרים בלבד");
                return;

            }
            if (!int.TryParse(amountOrderTB.Text, out x))
            {
                MessageBox.Show("נא להזין כמות תקינה");
                return;

            }
            if (WorkOrder.checkIfWorkOrderExist(int.Parse(orderNumTB.Text)))
            {
                MessageBox.Show("מס הוראה זה כבר קיים במערכת");
                return;
            }
            //(id,catalog,machine,amount,usercode,langcode,date)
            string query = $@"INSERT INTO WorkOrderItsik VALUES(?,?,?,?,?,?,?)";
            SqlParameter id = new SqlParameter("ca", int.Parse(orderNumTB.Text));
            SqlParameter date = new SqlParameter("da", DateTime.Now.ToString("yyyy-MM-dd"));
            SqlParameter uc = new SqlParameter("uc", int.Parse(userCodeTB.Text));
            SqlParameter lc = new SqlParameter("lc", int.Parse(langCodeTB.Text));
            SqlParameter catalog = new SqlParameter("lc", int.Parse(catalogOrderTB.Text));
            SqlParameter machineName = new SqlParameter("lc", machineSelection.Text);
            SqlParameter ammount = new SqlParameter("lc", int.Parse(amountOrderTB.Text));
            dbService.ExecuteQuery(query, CommandType.Text,id,catalog,machineName,ammount,uc,lc,date);
            MessageBox.Show("הוראה נוספה בהצלחה");
            catalogOrderTB.Text = "";
           // machineOrderTB.Text = "";
            amountOrderTB.Text = "";
            orderNumTB.Text = "";

        }
        private bool checkIfCataologNumExist(int num)
        {
            return Part.compareByCatalogNum(num);
        }
        private bool checkIfMachineExist(string s)
        {
            return Machine.checkIfMachineExist(s);
        }

        private void workOrderRepButton_Click(object sender, EventArgs e)
        {
            dataShow d = new dataShow("wo");
            d.Show();
        }

        private void partRepButton_Click(object sender, EventArgs e)
        {
            dataShow d = new dataShow("p");
            d.Show();
        }

        private void machineRepButton_Click(object sender, EventArgs e)
        {
            dataShow d = new dataShow("m");
            d.Show();
        }
        private void updateListOfMachine()
        {
            string query = $@"SELECT RTRIM(LTrim(name)) as a,left(name,6) as b,right(RTRIM(LTrim(name)),2) as d FROM MachineItsik order by d";
           // string query = $@"SELECT RTRIM(LTrim(nameFROM MachineItsik order by name";
            machineSelection.DataSource = dbService.GetDataSetByQuery(query, CommandType.Text).Tables[0];
            machineSelection.DisplayMember = "name";
            machineSelection.ValueMember = "a";
        }

        private void button1_Click(object sender, EventArgs ee)
        {
            DBService dbAS400 = new DBService();
            string query = $@"SELECT MACHINE,PDATE,EMPLOYEE,PRODUCTID FROM STWIND.PRRP WHERE DATE(PDATE)>=DATE('2018-08-15 00:00:00')";
            DataTable data= dbAS400.executeSelectQueryNoParam(query);
            query = $@"INSERT INTO MachineItsik VALUES(?,?,?,?)";
            int i = 0;
                foreach (DataRow row in data.Rows)
                {
                    SqlParameter machine = new SqlParameter("ma", row.ItemArray[0]);
                    SqlParameter date = new SqlParameter("da", ((DateTime)row.ItemArray[1]).ToString("yyyy-MM-dd"));
                    SqlParameter emp = new SqlParameter("em", int.Parse((string)row.ItemArray[2]));
                    SqlParameter prod = new SqlParameter("pr", int.Parse((string)row.ItemArray[3]));
                    dbService.ExecuteQuery(query, CommandType.Text, machine, date, emp, prod);
                    this.progressBar1.Value = ++i * 100 / data.Rows.Count;
               
                }
               
          

        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        private void makeExcelRep(string query)
        {
            progressBar1.Value = 0;
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
          //  Excel.Worksheet xlWorkSheet2;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartRange;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            // xlWorkSheet2 = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
            DataTable data = dbService.executeSelectQueryNoParam(query);
            xlWorkSheet.Cells[1, 1] = "מכונה";
            xlWorkSheet.Cells[1, 2] = "תאריך";
            xlWorkSheet.Cells[1, 3] = "עובד";
            xlWorkSheet.Cells[1, 4] = "מס' צמיג";
            //            xlWorkSheet.Application.ActiveWindow.FreezePanes = true;
            // Now apply autofilter
            Excel.Window xlWnd1 = xlApp.ActiveWindow;
            chartRange = xlWorkSheet.get_Range("A1", "A1").get_Offset(1, 0).EntireRow;
            chartRange.Select();
            xlWnd1.FreezePanes = true;
            Excel.Range firstRow = (Excel.Range)xlWorkSheet.Rows[1];
            firstRow.Application.ActiveWindow.FreezePanes = true;
            firstRow.AutoFilter(1,
                                Type.Missing,
                                Excel.XlAutoFilterOperator.xlAnd,
                                Type.Missing,
                                true);

            for (int i = 0; i< data.Rows.Count; i++)
            {
                xlWorkSheet.Cells[i+2, 1] = data.Rows[i].ItemArray[0];
                xlWorkSheet.Cells[i+2, 2] = data.Rows[i].ItemArray[1];
                xlWorkSheet.Columns[i + 2].ColumnWidth = 18;
                xlWorkSheet.Cells[i+2, 3] = data.Rows[i].ItemArray[2];
                xlWorkSheet.Cells[i+2, 4] = data.Rows[0].ItemArray[3];
                progressBar1.Value = i * 100 / data.Rows.Count;
            }

            //      xlApp.Visible = true;
            xlWorkBook.SaveAs("C:\\Users\\ielia\\Documents\\itsik.xls", Type.Missing, Type.Missing, Type.Missing,
            Type.Missing, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange,
            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
            Type.Missing);
            xlWorkBook.Close();
            releaseObject(xlApp);
            releaseObject(xlWorkBook);
            releaseObject(xlWorkSheet);


            MailMessage message = new MailMessage();
            message.Subject = "Itsik Excel file";
            message.Priority = MailPriority.High;
            message.Body = "Is it Working ?";
            message.To.Add(new MailAddress("ielia@atgtire.com"));
            message.From = new MailAddress("ielia@atgtire.com");
            Attachment attach1 = new Attachment(@"C:\\Users\\ielia\\Documents\\itsik.xls");
            message.Attachments.Add(attach1);
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            SmtpClient client = new SmtpClient();
            client.Host = "almail";// ServerIP;
            client.Send(message);
            client.Dispose();
            progressBar1.Value = progressBar1.Maximum;






            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            makeExcelRep("SELECT top(100) * FROM MachineItsik");

        }
    }
}

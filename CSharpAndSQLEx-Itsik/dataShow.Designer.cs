﻿namespace CSharpAndSQLEx_Itsik
{
    partial class dataShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataShowing = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataShowing)).BeginInit();
            this.SuspendLayout();
            // 
            // dataShowing
            // 
            this.dataShowing.AllowUserToDeleteRows = false;
            this.dataShowing.AllowUserToOrderColumns = true;
            this.dataShowing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataShowing.EnableHeadersVisualStyles = false;
            this.dataShowing.Location = new System.Drawing.Point(6, 1);
            this.dataShowing.Name = "dataShowing";
            this.dataShowing.RowHeadersVisible = false;
            this.dataShowing.Size = new System.Drawing.Size(185, 102);
            this.dataShowing.TabIndex = 1;
            // 
            // dataShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(193, 105);
            this.Controls.Add(this.dataShowing);
            this.Name = "dataShow";
            this.Text = "dataShow";
            this.Load += new System.EventHandler(this.dataShow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataShowing)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataShowing;
    }
}
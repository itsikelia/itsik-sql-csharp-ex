﻿namespace CSharpAndSQLEx_Itsik
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.partButton = new System.Windows.Forms.Button();
            this.machineButton = new System.Windows.Forms.Button();
            this.workOrderButton = new System.Windows.Forms.Button();
            this.userCodeTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.langCodeTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.catalogTB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.partDescTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.machineNameTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.orderNumTB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.catalogOrderTB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.amountOrderTB = new System.Windows.Forms.TextBox();
            this.workOrderRepButton = new System.Windows.Forms.Button();
            this.partRepButton = new System.Windows.Forms.Button();
            this.machineRepButton = new System.Windows.Forms.Button();
            this.machineSelection = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // partButton
            // 
            this.partButton.Location = new System.Drawing.Point(519, 356);
            this.partButton.Name = "partButton";
            this.partButton.Size = new System.Drawing.Size(75, 23);
            this.partButton.TabIndex = 0;
            this.partButton.Text = "הוסף פריט";
            this.partButton.UseVisualStyleBackColor = true;
            this.partButton.Click += new System.EventHandler(this.partButton_Click);
            // 
            // machineButton
            // 
            this.machineButton.Location = new System.Drawing.Point(289, 356);
            this.machineButton.Name = "machineButton";
            this.machineButton.Size = new System.Drawing.Size(75, 23);
            this.machineButton.TabIndex = 1;
            this.machineButton.Text = "הוסף מכונה";
            this.machineButton.UseVisualStyleBackColor = true;
            this.machineButton.Click += new System.EventHandler(this.machineButton_Click);
            // 
            // workOrderButton
            // 
            this.workOrderButton.Location = new System.Drawing.Point(43, 356);
            this.workOrderButton.Name = "workOrderButton";
            this.workOrderButton.Size = new System.Drawing.Size(95, 23);
            this.workOrderButton.TabIndex = 2;
            this.workOrderButton.Text = "הוסף הוראה";
            this.workOrderButton.UseVisualStyleBackColor = true;
            this.workOrderButton.Click += new System.EventHandler(this.workOrderButton_Click);
            // 
            // userCodeTB
            // 
            this.userCodeTB.Location = new System.Drawing.Point(233, 28);
            this.userCodeTB.Name = "userCodeTB";
            this.userCodeTB.Size = new System.Drawing.Size(100, 20);
            this.userCodeTB.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(357, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "קוד משתמש";
            // 
            // langCodeTB
            // 
            this.langCodeTB.Location = new System.Drawing.Point(233, 54);
            this.langCodeTB.Name = "langCodeTB";
            this.langCodeTB.Size = new System.Drawing.Size(100, 20);
            this.langCodeTB.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(373, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "קוד שפה";
            // 
            // catalogTB
            // 
            this.catalogTB.Location = new System.Drawing.Point(475, 237);
            this.catalogTB.Name = "catalogTB";
            this.catalogTB.Size = new System.Drawing.Size(100, 20);
            this.catalogTB.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(603, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "מק\"ט";
            // 
            // partDescTB
            // 
            this.partDescTB.Location = new System.Drawing.Point(475, 273);
            this.partDescTB.Name = "partDescTB";
            this.partDescTB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.partDescTB.Size = new System.Drawing.Size(100, 20);
            this.partDescTB.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(603, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "תיאור פריט";
            // 
            // machineNameTB
            // 
            this.machineNameTB.Location = new System.Drawing.Point(233, 234);
            this.machineNameTB.Name = "machineNameTB";
            this.machineNameTB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.machineNameTB.Size = new System.Drawing.Size(100, 20);
            this.machineNameTB.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(357, 237);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "שם מכונה";
            // 
            // orderNumTB
            // 
            this.orderNumTB.Location = new System.Drawing.Point(12, 231);
            this.orderNumTB.Name = "orderNumTB";
            this.orderNumTB.Size = new System.Drawing.Size(100, 20);
            this.orderNumTB.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(139, 234);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "מס\' הוראה";
            // 
            // catalogOrderTB
            // 
            this.catalogOrderTB.Location = new System.Drawing.Point(12, 266);
            this.catalogOrderTB.Name = "catalogOrderTB";
            this.catalogOrderTB.Size = new System.Drawing.Size(100, 20);
            this.catalogOrderTB.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(165, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "מק\"ט";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(141, 301);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "שם מכונה";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(141, 333);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "כמות פריטים";
            // 
            // amountOrderTB
            // 
            this.amountOrderTB.Location = new System.Drawing.Point(12, 330);
            this.amountOrderTB.Name = "amountOrderTB";
            this.amountOrderTB.Size = new System.Drawing.Size(100, 20);
            this.amountOrderTB.TabIndex = 20;
            // 
            // workOrderRepButton
            // 
            this.workOrderRepButton.Location = new System.Drawing.Point(519, 13);
            this.workOrderRepButton.Name = "workOrderRepButton";
            this.workOrderRepButton.Size = new System.Drawing.Size(149, 33);
            this.workOrderRepButton.TabIndex = 21;
            this.workOrderRepButton.Text = "דו\"ח הוראות";
            this.workOrderRepButton.UseVisualStyleBackColor = true;
            this.workOrderRepButton.Click += new System.EventHandler(this.workOrderRepButton_Click);
            // 
            // partRepButton
            // 
            this.partRepButton.Location = new System.Drawing.Point(520, 51);
            this.partRepButton.Name = "partRepButton";
            this.partRepButton.Size = new System.Drawing.Size(148, 35);
            this.partRepButton.TabIndex = 22;
            this.partRepButton.Text = "דו\"ח פריטים";
            this.partRepButton.UseVisualStyleBackColor = true;
            this.partRepButton.Click += new System.EventHandler(this.partRepButton_Click);
            // 
            // machineRepButton
            // 
            this.machineRepButton.Location = new System.Drawing.Point(519, 92);
            this.machineRepButton.Name = "machineRepButton";
            this.machineRepButton.Size = new System.Drawing.Size(147, 39);
            this.machineRepButton.TabIndex = 23;
            this.machineRepButton.Text = "דו\"ח מכונות";
            this.machineRepButton.UseVisualStyleBackColor = true;
            this.machineRepButton.Click += new System.EventHandler(this.machineRepButton_Click);
            // 
            // machineSelection
            // 
            this.machineSelection.FormattingEnabled = true;
            this.machineSelection.Location = new System.Drawing.Point(12, 298);
            this.machineSelection.Name = "machineSelection";
            this.machineSelection.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.machineSelection.Size = new System.Drawing.Size(100, 21);
            this.machineSelection.TabIndex = 24;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 60);
            this.button1.TabIndex = 25;
            this.button1.Text = "ביצוע משימה של מג\'ד";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(31, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(167, 44);
            this.button2.TabIndex = 26;
            this.button2.Text = "הראה דוח באקסל";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(58, 409);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(578, 23);
            this.progressBar1.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 444);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.machineSelection);
            this.Controls.Add(this.machineRepButton);
            this.Controls.Add(this.partRepButton);
            this.Controls.Add(this.workOrderRepButton);
            this.Controls.Add(this.amountOrderTB);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.catalogOrderTB);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.orderNumTB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.machineNameTB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.partDescTB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.catalogTB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.langCodeTB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.userCodeTB);
            this.Controls.Add(this.workOrderButton);
            this.Controls.Add(this.machineButton);
            this.Controls.Add(this.partButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button partButton;
        private System.Windows.Forms.Button machineButton;
        private System.Windows.Forms.Button workOrderButton;
        private System.Windows.Forms.TextBox userCodeTB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox langCodeTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox catalogTB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox partDescTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox machineNameTB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox orderNumTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox catalogOrderTB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox amountOrderTB;
        private System.Windows.Forms.Button workOrderRepButton;
        private System.Windows.Forms.Button partRepButton;
        private System.Windows.Forms.Button machineRepButton;
        private System.Windows.Forms.ComboBox machineSelection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}


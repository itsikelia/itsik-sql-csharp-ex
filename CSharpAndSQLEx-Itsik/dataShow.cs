﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSharpAndSQLEx_Itsik
{
    public partial class dataShow : Form
    {
        string rep="";
        public dataShow(string whichRep)
        {
            InitializeComponent();
            rep = whichRep;
            this.AutoSize = true;
            dataShowing.AutoSize = true;
            dataShowing.BackgroundColor = this.BackColor;
        }

        private void dataShow_Load(object sender, EventArgs e)
        {
            string query="";
            switch (rep)
            {
                case "wo": query = $@"SELECT * FROM [Test].[dbo].[WorkOrderItsik] order by dateCreated DESC";this.Text="Work Order Report"; break;
                case "p": query = $@"SELECT * FROM [Test].[dbo].[PartItsik] order by catalogNum"; this.Text = "Part Report"; break;
                case "m": query= $@"SELECT * FROM [Test].[dbo].[MachineItsik] order by right(RTRIM(LTrim(name)),2)"; this.Text = "Machine Report"; break;
            }
          
            dataShowing.DataSource=Form1.dbService.GetDataSetByQuery(query, CommandType.Text).Tables[0];
          
        }
    }
}

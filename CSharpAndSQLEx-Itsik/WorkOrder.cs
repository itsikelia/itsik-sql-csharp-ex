﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAndSQLEx_Itsik
{
    class WorkOrder:Yeshut
    {
        public WorkOrder(int userCode,int langCode,int workOrderId, int catalogToCreate, Machine creatingMachine, int amountToProduce)
        {
            this.workOrderId = workOrderId;
            this.catalogToCreate = catalogToCreate;
            this.creatingMachine = creatingMachine;
            this.amountToProduce = amountToProduce;
            this.dateCreated = DateTime.Now;
            this.userCode = userCode;
            this.languageCode = langCode;
        }

        public int workOrderId { get; set; }
        public int catalogToCreate { get; set; }
        public Machine creatingMachine { get; set; }
        public int amountToProduce { get; set; }

        public static bool checkIfWorkOrderExist(int num)
        {
            string query = $@"SELECT COUNT(*)
                             FROM [Test].[dbo].[WorkOrderItsik]
                              WHERE id=" + num  ;
            return Form1.dbService.GetScalarByQuery(query, System.Data.CommandType.Text) > 0;
        }

    }
}
